/*
 * Copyright (C) 2015 Naresh Mehta https://www.naresh.se/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * Project hosted at:
 * https://bitbucket.org/wolverine2k/videolog
 *
 * FileName: MyCameraImpl.java
 */

package se.naresh.com.videolog;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;

/**
 * Created by Naresh Mehta on 2015-03-17.
 */
/* TODO: Differentiate between AP15 and API21.
* TODO: Only API15 is implemented, Implement API21 i.e. camera2 interface */
public class MyCameraImpl {
    private final static String LOG_TAG = "MyCameraImpl";
    private static Context appContext = null;
    private MyCameraImpl.CameraImplParameters logParams; /* Just so we keep track of logParams in this class */
    private Camera deviceCam_API15 = null;

    /* Used for versions prior to Lollipop */
    private Camera camera;
    /* For versions Lollipop & above */
    private CameraManager cameraManager;

    /* Public Constructor */
    public MyCameraImpl(Context context) {
        appContext = context;
    }

    /* Set the previewDisplay for the Camera */
    public void setPreviewDisplay(SurfaceHolder holder) {
        try {
            deviceCam_API15.setPreviewDisplay(holder);
        } catch (IOException e) {
            releaseCamera();
            Log.e(LOG_TAG, "Error setting Camera Preview API15..." + e.getMessage());
            e.printStackTrace();
        }
    }

    public void stopPreview() {
        deviceCam_API15.stopPreview();
    }

    /**
     * Release the Camera resource
     */
    public void releaseCamera() {
        if(deviceCam_API15 != null) {
            deviceCam_API15.release();
            deviceCam_API15 = null;
        }
        /* TODO: Implement the API21 CameraRelease functionality */
    }

    /**
     * Safely open the camera available on the device
     */
    public boolean openCamera(CAMERA_TYPE typeOfCamera) {
        boolean result = false;
        try {
            releaseCamera();
            /* TODO: For the time being we open only the front camera
            * Logic TBD for opening camera's based on
            * 1. API level
            * 2. Preference
            * */
            deviceCam_API15 = Camera.open(Integer.parseInt(logParams.frontCameraID));
        } catch(Exception e) {
            Log.e(LOG_TAG, "Error Opening Device Camera..." + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Check's the camera options available on the device
     */
    public boolean checkCameraOptions(CameraImplParameters aLogParams) {
        logParams = aLogParams;
        boolean result = false;
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
                                    && ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP))) {
            Log.d(LOG_TAG, "Older versions running. Calling API15()...");
            result = checkCameraOptionsAPI15(logParams);
         } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /* Use newer APIs if we are using Lollipop and above */
            Log.d(LOG_TAG, "Lollipop or above running. Calling API21()...");
            result = checkCameraOptionsAPI21(logParams);
         }
        return result;
    }

    /**
     * Returns true if Camera/Video logging feature is available else will
     * proceed with Audio/Voice logging only. It also stores the cameraIDs
     * for front and back facing cameras (if available).
     * Only available for devices between API 15 and API 20 i.e. < Lollipop
     */
    private boolean checkCameraOptionsAPI15(CameraImplParameters aLogParams) {
        aLogParams.isVideoLog = false;
        if(appContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            /* Great Device has a Camera */
            aLogParams.isVideoLog = true;
            aLogParams.hasFrontCamera = false;
            try {
                for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.CameraInfo info = new Camera.CameraInfo();
                    Camera.getCameraInfo(i, info);
                    if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                        Log.d(LOG_TAG, "Front Facing CameraID : " + i);
                        aLogParams.frontCameraID = Integer.toString(i);
                        aLogParams.hasFrontCamera = true;
                    } else if(info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                        Log.d(LOG_TAG, "Back Facing CameraID : " + i);
                        aLogParams.backCameraID = Integer.toString(i);
                    }
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "Exception while getting CameraInfo() API15" + e.getMessage());
                e.printStackTrace();
            }
        } else {
            /* Device does not have a Camera. We will do voice logging only */
            aLogParams.isVideoLog = false;
        }
        Log.d(LOG_TAG, "Camera Present : " + ((aLogParams.isVideoLog)?"YES":"NO"));
        return aLogParams.isVideoLog;
    }

    /**
     * Returns true if Camera/Video logging feature is available else will
     * proceed with Audio/Voice logging only. It also stores the cameraIDs
     * for front and back facing cameras (if available).
     * Only available for devices featuring Lollipop and above...
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private boolean checkCameraOptionsAPI21(CameraImplParameters aLogParams) {
        aLogParams.isVideoLog = false;
        if(appContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            /* Great Device has a Camera */
            aLogParams.isVideoLog = true;
            aLogParams.hasFrontCamera = false;
            CameraManager manager = (CameraManager) appContext.getSystemService(Context.CAMERA_SERVICE);
            try {
                for (final String cameraId : manager.getCameraIdList()) {
                    CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                    int facingLens = characteristics.get(CameraCharacteristics.LENS_FACING);
                    if (CameraCharacteristics.LENS_FACING_FRONT == facingLens) {
                        Log.d(LOG_TAG, "Front Facing CameraID : " + cameraId);
                        aLogParams.frontCameraID = cameraId;
                        aLogParams.hasFrontCamera = true;
                    } else if(CameraCharacteristics.LENS_FACING_BACK == facingLens) {
                        Log.d(LOG_TAG, "Back Facing CameraID: " + cameraId);
                        aLogParams.frontCameraID = cameraId;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "Exception while getting CameraIDList() API21" + e.getMessage());
                e.printStackTrace();
            }
        } else {
            /* Device does not have a Camera. We will do voice logging only */
            aLogParams.isVideoLog = false;
        }
        Log.d(LOG_TAG, "Camera Present : " + ((aLogParams.isVideoLog)?"YES":"NO"));
        return aLogParams.isVideoLog;
    }

    /**
     * A data structure to store the logging parameters to use
     */
    public static class CameraImplParameters {
        public boolean isVideoLog = false;
        public boolean hasFrontCamera = false;
        public String frontCameraID = null;
        public String backCameraID = null;
    }

    /**
     * An enum to give a preference to the CAMERA_TYPE
     * Generally AVAILABLE_DEVICE is a great option but this can be set/get from the
     * Application Settings if any
     */
    public static enum CAMERA_TYPE {
        FRONT_CAMERA,
        BACK_CAMERA,
        AUDIO_ONLY,
        AVAILABLE_DEVICE, /* Priority is in the above order, i.e. FRONT, BACK & AUDIO */
    }
}
