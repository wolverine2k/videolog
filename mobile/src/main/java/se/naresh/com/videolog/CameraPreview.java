/*
 * Copyright (C) 2015 Naresh Mehta https://www.naresh.se/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * Project hosted at:
 * https://bitbucket.org/wolverine2k/videolog
 *
 * FileName: CameraPreview.java
 */
package se.naresh.com.videolog;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Naresh Mehta on 2015-03-04.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private final static String LOG_TAG = "CameraPreview";
    private SurfaceHolder surfaceHolder;
    private Context baseContext = null;

    private MyCameraImpl.CameraImplParameters logParams = new MyCameraImpl.CameraImplParameters();
    private MyCameraImpl cameraImpl = null;

    /* TODO: Try to optimize variables */
    private int surfaceChangedCallDepth = 0;
    protected boolean surfaceConfiguring = false;

    public CameraPreview(Context context) {
        super(context);

        baseContext = context;
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        /* No need to setType as the value is set automatically when needed
         * Not needed since we are supporting a minimum of API 15
         * REF: http://developer.android.com/reference/android/view/SurfaceHolder.html
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        */
        cameraImpl = new MyCameraImpl(context);
        cameraImpl.checkCameraOptions(logParams);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        cameraImpl.setPreviewDisplay(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        surfaceChangedCallDepth++;
        doSurfaceChanged(width, height);
        surfaceChangedCallDepth--;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        cameraImpl.stopPreview();
        cameraImpl.releaseCamera();
    }

    /* Returns true if current orientation is portrait
     * Videologging is better in portrait ;) */
    public boolean isPortrait() {
        return (baseContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    /*
     * Takes care of preview changes and rotation
     * Basically, we stop the preview before resizing or reformatting it
     * and start it up again
     */
    private void doSurfaceChanged(int width, int height) {
        cameraImpl.stopPreview();
        boolean portrait = isPortrait();
        if(!surfaceConfiguring) {

        }
    }
}
