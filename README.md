# README #

The aim of **VideoLog** as an application is to run on multiple platforms and provide a video-logging functionality (something similar to the VideoLogs in **Aavatar!**). I had selected [CodeNameOne (CN1)](http://www.codenameone.com/) as the cross platform library of choice. It has many plus points and I see a lot of potential in it but it makes little sense when the application is going to work closely with either device HW or native device components. I realized quite soon that I will need to re-write

* Most of the container controls to suite my test eg: MediaPlayer, Button, etc.
* Some of the things like capturing repeated keystrokes has a workaround which is very tedious and that too needs to be implemented in the platform dependent code.

So I have decided not to use CN1 for this project. For the time being, this project will migrate to native Android APIs and will be launched on Android first.

**CN1 code is still residing in *CodeName1_Abandoned* branch**, which can be looked up as a reference for CN1 usage as well as an example demo. VideoCapture and playback is working as expected. Of course corner cases or race conditions are not handled and that is in no way a complete application. **CN1 has great powers** and I will try to contribute to it as I get along with it. My aim is to use CN1 for the OrdnaFIKA project though.

### What is this repository for? ###

* VideoLog is an application that will do VideoLog recording and sharing to Social Networks (aka Aavtar Style)! 
* 0.0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Android Studio (1.1.0 or up), Android SDK
* Configuration -- TBD
* Dependencies -- Android SDK, Gradle
* Database configuration -- TBD
* How to run tests -- TBD
* Deployment instructions -- Will be available on PlayStore

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact